﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour, InterestedInPlayerInterace
{

    private Rigidbody2D rig;
    private GameObject player;
    private bool allowedToMove;
    private float speedMultiplier;
    private float waitTime = 1.6f;
    private Animator ani;
    private Vector3 transfromSave;

    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player");
        ani = GetComponent<Animator>();
        Camera.main.GetComponent<TurnBasedController>().AddTolist(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            Move();


        if (allowedToMove)
        {
            transform.Translate(Vector3.left * speedMultiplier * Time.deltaTime);
        }
    }

    public void Move()
    {
        if (Vector3.Distance(player.transform.position, transform.position) > 35)
            return;
        ani.SetBool("Moving", true);
        allowedToMove = true;
        MoveFast();
        Invoke("StopMoving", waitTime);
    }

    public void StopMoving()
    {
        allowedToMove = false;
        ani.SetBool("Moving", false);


    }


    private void MoveNormally()
    {
        speedMultiplier = 100f / (waitTime * 10);
    }

    private void MoveFast()
    {
        speedMultiplier = 200f / (waitTime * 10);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerController>().Die();
        }
    }

    void InterestedInPlayerInterace.SetNewPlayer(GameObject player)
    {
        this.player = player;
    }

    public void Save()
    {
        transfromSave = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    public void Load()
    {
        StopMoving();
        transform.position = transfromSave;
    }
}
