﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartTextScript : MonoBehaviour {
    private TurnBasedController turnBasedController;
    private Text text;

    // Use this for initialization
    void Start()
    {
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (turnBasedController.GetCurrentState() != Constants.States.START)
            text.text = "";
    }
}
