﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSavedTextScript : MonoBehaviour {

    private Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - Time.deltaTime/6);
	}

    public void ShowText()
    {
        text.color = Color.black;
    }
}
