﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{


    private GameObject player;
    private bool allowedToMove;
    private Animator ani;
    private int counter;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
        ani = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            Move();

    }

    public void Move()
    {
        NextStep();
    }

    private void NextStep()
    {
        switch (counter)
        {
            case 0:
                ani.SetTrigger("Shake");
                break;
            case 1:
                break;
            case 2:
                ani.SetTrigger("Kill");

                counter = -1;
                break;
            default: break;
        }

        counter++;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<PlayerController>() != null)
                collision.gameObject.GetComponent<PlayerController>().Die();
            else
                collision.gameObject.GetComponentInParent<PlayerController>().Die();
        }
    }

}
