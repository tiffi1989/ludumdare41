﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour, InterestedInPlayerInterace
{

    private Transform player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player").transform;
        Camera.main.GetComponent<TurnBasedController>().AddTolist(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < player.position.x + 17.9f)
            transform.position = new Vector3(transform.position.x + Time.deltaTime * 8, player.position.y + 5, -10);
        else if (Input.GetKey(KeyCode.RightArrow))
            transform.position = new Vector3(player.position.x + 18, player.position.y + 5, -10);
        else if (transform.position.x > player.position.x + 5.1f)
            transform.position = new Vector3(transform.position.x - Time.deltaTime * 8, player.position.y + 5, -10);
        else
            transform.position = new Vector3(player.position.x + 5, player.position.y + 5, -10);
    }

    public void SetNewPlayer(GameObject player)
    {
        this.player = player.transform;
    }

    public void Save()
    {
    }

    public void Load()
    {
        transform.position = new Vector3(player.position.x + 5, player.position.y + 5, -10);
    }
}
