﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnBasedController : MonoBehaviour, InterestedInPlayerInterace
{


    private Constants.States currentState;

    private GameObject player;
    public GameObject playerPrefab;
    private List<InterestedInPlayerInterace> interestedInPlayer = new List<InterestedInPlayerInterace>();
    private List<InterestedInPlayerInterace> toDelete = new List<InterestedInPlayerInterace>();
    private float xOfLastCheckPoint;

    // Use this for initialization
    void Start()
    {
        currentState = Constants.States.START;
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (currentState)
        {
            case Constants.States.START:
                HandleStart();
                break;
            case Constants.States.PLAYERCHOOSING:
                HandlePlayerChoosing();
                break;
            case Constants.States.ALLMOVESSET:
                HandleAllMovesSet();
                break;
            case Constants.States.PLAYERMOVING:
                HandlePlayerMoving();
                break;
            case Constants.States.PLAYERMOVEMENTDONE:
                HandlePlayerMovementDone();
                break;
            case Constants.States.PLAYERDEAD:
                HandlePlayerDead();
                break;
            case Constants.States.END:
                HandleEnd();
                break;
            default: break;
        }

    }

    private void HandleStart() {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Save();
            currentState = Constants.States.PLAYERCHOOSING;
        }

    }

    private void HandlePlayerChoosing() { }

    private void HandleAllMovesSet()
    {
        SetState(Constants.States.PLAYERMOVING);
    }

    private void HandlePlayerMoving()
    {

    }

    private void HandlePlayerMovementDone()
    {
        SetState(Constants.States.PLAYERCHOOSING);
    }

    private void HandlePlayerDead()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            Load();
        }
    }

    private void HandleEnd()
    {

    }

    public void SetState(Constants.States state)
    {
        currentState = state;
        Debug.Log("Switched to State " + state);
        foreach (InterestedInPlayerInterace interested in toDelete)
        {
            interestedInPlayer.Remove(interested);
        }
        toDelete = new List<InterestedInPlayerInterace>();
    }

    public Constants.States GetCurrentState()
    {
        return currentState;
    }

    public void setNewPlayer(GameObject player)
    {
        this.player = player;
    }

    public void SetNewPlayer(GameObject player)
    {
        this.player = player;
    }

    public void AddTolist(InterestedInPlayerInterace interestedScript)
    {
        interestedInPlayer.Add(interestedScript);
    }

    public void Save()
    {
        Debug.Log("State Saved");

        foreach (InterestedInPlayerInterace interested in interestedInPlayer)
        {
            if (interested != null)
                interested.Save();
        }
    }

    public void RemoveFromList(GameObject go)
    {
        toDelete.Add(go.GetComponent<InterestedInPlayerInterace>());
    }


    public void Load()
    {
        Debug.Log("Loaded State");
        GameObject newPlayer = Instantiate(playerPrefab, new Vector3(xOfLastCheckPoint, -2, 0), Quaternion.identity);
        Destroy(player);
        player = newPlayer;
        foreach (InterestedInPlayerInterace interested in interestedInPlayer)
        {
            Debug.Log("interested " + interested);
            if (interested != null)
            {
                interested.SetNewPlayer(newPlayer);
                interested.Load();

            }
        }

        currentState = Constants.States.PLAYERCHOOSING;
    }

    public void SetXOfLastCheckpoint(float x)
    {
        xOfLastCheckPoint = x;
    }
}
