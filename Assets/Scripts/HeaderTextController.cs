﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HeaderTextController : MonoBehaviour {

    private TurnBasedController turnBasedController;
    private Text text;

	// Use this for initialization
	void Start () {
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		switch(turnBasedController.GetCurrentState())
        {
            case Constants.States.START: break;
            case Constants.States.PLAYERCHOOSING: text.text = "Press WASD + Return to select a Move";
                break;
            case Constants.States.PLAYERMOVING: text.text = "Lets see...";
                break;
            case Constants.States.ALLMOVESSET: break;
            case Constants.States.PLAYERDEAD: text.text = "Too bad... Press R to start from the last checkpoint.";
                break;
            case Constants.States.END: text.text = "You win.";
                break;
            default:
                text.text = "";
                break;


        }
    }
}
