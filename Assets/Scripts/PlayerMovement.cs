﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;


public class PlayerMovement : MonoBehaviour
{

    private TurnBasedController turnBasedController;
    private bool movementInProgress, waiting;
    private Rigidbody2D playerRig;
    private PlayerController playerController;
    public float speed, jumpforce, waitTime;
    private Animator playerAni;
    private UnityEvent playerMoveEvent;
    private long timeAtStart;
    public AudioClip walking;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        if (playerMoveEvent == null)
            playerMoveEvent = new UnityEvent();
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        playerRig = GetComponent<Rigidbody2D>();
        playerController = GetComponent<PlayerController>();
        playerAni = GetComponent<Animator>();
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if(enemy.GetComponent<CatController>() != null)
                playerMoveEvent.AddListener(enemy.GetComponent<CatController>().Move);
            if (enemy.GetComponent<BatController>() != null)
                playerMoveEvent.AddListener(enemy.GetComponent<BatController>().Move);

            if (enemy.GetComponent<RockController>() != null)
                playerMoveEvent.AddListener(enemy.GetComponent<RockController>().Move);
            if (enemy.GetComponent<SpikeTrap>() != null)
                playerMoveEvent.AddListener(enemy.GetComponent<SpikeTrap>().Move);

        };

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        IsMovementInProgress();

        if (turnBasedController.GetCurrentState() == Constants.States.PLAYERMOVING && !movementInProgress)
        {
            movementInProgress = true;
            Constants.Moves nextMove = playerController.GetNextMove();
            if (nextMove != Constants.Moves.EMPTY)
                MovePlayer(nextMove);
            else
            {
                turnBasedController.SetState(Constants.States.PLAYERMOVEMENTDONE);
            }
        }

    }

    void IsMovementInProgress()
    {
        if (playerRig.velocity.magnitude == 0 && !waiting)
        {
            movementInProgress = false;
            if(timeAtStart > 0)
            {
                Debug.Log("Time for Move = " + (((long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds - timeAtStart)));
                timeAtStart = 0;
            }
        }
    }

    void MovePlayer(Constants.Moves move)
    {
        playerMoveEvent.Invoke();
        timeAtStart = (long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;
        switch (move)
        {
            case Constants.Moves.LEFT:
            case Constants.Moves.RIGHT:
                Run(move);
                break;

            case Constants.Moves.DOWN:
                Duck();
                break;
            case Constants.Moves.UPLEFT:
            case Constants.Moves.UP:
            case Constants.Moves.UPRIGHT:
                Jump(move);
                break;
            case Constants.Moves.SLIDELEFT:
            case Constants.Moves.SLIDERIGHT:
                Slide(move);
                break;
            case Constants.Moves.DONOTHING:
                DoNothing();
                break;
            default:
                break;
        }

    }

    void Slide(Constants.Moves move)
    {
        playerAni.SetTrigger("Slide");

        switch (move)
        {
            case Constants.Moves.SLIDELEFT:
                playerRig.AddForce(new Vector2(-speed * 1.3222f, 0));
                Look(false);

                break;

            case Constants.Moves.SLIDERIGHT:
                playerRig.AddForce(new Vector2(speed * 1.3222f, 0));
                Look(true);

                break;
            default: break;

        }
    }

    void Jump(Constants.Moves move)
    {

        playerAni.SetTrigger("Jump");
        switch (move)
        {
            case Constants.Moves.UPLEFT:
                playerRig.AddForce(new Vector2(-speed, jumpforce));
                Look(false);

                break;

            case Constants.Moves.UP:
                playerRig.AddForce(new Vector2(0, jumpforce));

                break;

            case Constants.Moves.UPRIGHT:
                playerRig.AddForce(new Vector2(speed, jumpforce));
                Look(true);

                break;
            default: break;
        }
    }

    void Run(Constants.Moves move)
    {
        playerAni.SetTrigger("Run");
        audioSource.clip = walking;
        audioSource.Play();


        switch (move)
        {
            case Constants.Moves.LEFT:
                playerRig.AddForce(new Vector2(-speed * 1.3222f, 0));
                Look(false);
                break;

            case Constants.Moves.RIGHT:
                playerRig.AddForce(new Vector2(speed * 1.3222f, 0));
                Look(true);

                break;
            default: break;

        }
    }

    void Duck()
    {
        playerAni.SetTrigger("Duck");

        waiting = true;
        Invoke("StopWaiting", waitTime-.1f);
    }

    void DoNothing()
    {
        waiting = true;
        Invoke("StopWaiting", waitTime);
    }

    void StopWaiting()
    {
        waiting = false;
    }

    private void Look(bool right)
    {
        if (right)
            transform.localScale = new Vector3(1, 1, 1);
        else
            transform.localScale = new Vector3(-1, 1, 1);

    }
}
