﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderController : MonoBehaviour, InterestedInPlayerInterace
{

    private GameObject arrow, x, slide;
    private Transform arrowTransform, xTransform, slideTransform;
    public int number;
    private PlayerController player;
    private TurnBasedController turnBasedController;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        arrow = transform.Find("ArrowUp").gameObject;
        x = transform.Find("X").gameObject;
        slide = transform.Find("SlideArrow").gameObject;
        arrowTransform = arrow.GetComponent<Transform>();
        xTransform = x.GetComponent<Transform>();
        slideTransform = slide.GetComponent<Transform>();
        Camera.main.GetComponent<TurnBasedController>().AddTolist(this);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (turnBasedController.GetCurrentState() == Constants.States.START || turnBasedController.GetCurrentState() == Constants.States.PLAYERDEAD)
            return;


        Constants.Moves move = player.GetMove(number);



        switch (move)
        {
            case Constants.Moves.LEFT:
                EnableRenderer(true, false, false);
                arrowTransform.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case Constants.Moves.RIGHT:
                EnableRenderer(true, false, false);
                arrowTransform.rotation = Quaternion.Euler(0, 0, -90);


                break;
            case Constants.Moves.UP:
                EnableRenderer(true, false, false);
                arrowTransform.rotation = Quaternion.Euler(0, 0, 0);

                break;
            case Constants.Moves.DOWN:
                EnableRenderer(true, false, false);
                arrowTransform.rotation = Quaternion.Euler(0, 0, 180);


                break;
            case Constants.Moves.UPLEFT:
                EnableRenderer(true, false, false);
                arrowTransform.rotation = Quaternion.Euler(0, 0, 45);


                break;
            case Constants.Moves.UPRIGHT:
                EnableRenderer(true, false, false);
                arrowTransform.rotation = Quaternion.Euler(0, 0, -45);

                break;
            case Constants.Moves.SLIDELEFT:
                EnableRenderer(false, false, true);
                slideTransform.localScale = new Vector3(-.5f, .5f, .5f);

                break;
            case Constants.Moves.SLIDERIGHT:
                EnableRenderer(false, false, true);
                slideTransform.localScale = new Vector3(.5f, .5f, .5f);

                break;
            case Constants.Moves.DONOTHING:
                EnableRenderer(false, true, false);
                break;
            default:
                EnableRenderer(false, false, false);
                break;
        }
    }

    private void EnableRenderer(bool arrowBool, bool xBool, bool slideBool)
    {
        arrow.SetActive(arrowBool);
        x.SetActive(xBool);
        slide.SetActive(slideBool);
    }

    public void SetNewPlayer(GameObject player)
    {
        this.player = player.GetComponent<PlayerController>();
    }

    public void Save()
    {
    }

    public void Load()
    {
    }
}
