﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface InterestedInPlayerInterace {

    void SetNewPlayer(GameObject player);

    void Save();

    void Load();
}
