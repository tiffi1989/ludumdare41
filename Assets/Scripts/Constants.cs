﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {

    public enum States
    {
        START = 0,
        PLAYERCHOOSING,
        ALLMOVESSET,
        PLAYERMOVING,
        PLAYERMOVEMENTDONE,
        PLAYERDEAD,
        END
    }

    public enum Moves
    {
        EMPTY = 0,
        LEFT,
        RIGHT,
        UP,
        DOWN,
        UPLEFT,
        UPRIGHT,
        SLIDELEFT,
        SLIDERIGHT,
        DONOTHING
        
    }
}
