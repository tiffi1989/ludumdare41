﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Constants.Moves[] moves;
    private TurnBasedController turnBasedController;
    private Constants.Moves[] movesTemp;
    private int currentMoveCounter = 0;
    private List<Constants.Moves> allPastMoves = new List<Constants.Moves>();
    private ParticleSystem blood;
    public GameObject head;
    private bool dead;
    private Animator ani;

    // Use this for initialization
    void Start () {
        moves = new Constants.Moves[4];
        movesTemp = new Constants.Moves[4];
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        blood = GetComponentInChildren<ParticleSystem>();
        ani = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetNextMove()
    {
        if (moves[currentMoveCounter] == Constants.Moves.EMPTY)
            return;

        currentMoveCounter++;
        
        if(currentMoveCounter > 3)
        {
            movesTemp = (Constants.Moves[])moves.Clone();
            foreach(Constants.Moves move in moves)
            {
                allPastMoves.Add(move);
            }
            currentMoveCounter = 0;
            turnBasedController.SetState(Constants.States.ALLMOVESSET);            
        }
    }

    public void SetCurrentMoveTemp(Constants.Moves move)
    {
        moves[currentMoveCounter] = move;
    }

    public Constants.Moves GetNextMove()
    {
        Constants.Moves nextMove = Constants.Moves.EMPTY;
        int counter = 0;
        foreach (Constants.Moves move in moves)
        {
            if(move != Constants.Moves.EMPTY)
            {
                nextMove = move;
                moves[counter] = Constants.Moves.EMPTY;
                break;
            }
            counter++;
        }

        return nextMove;
    }

    public int GetAmountOfSetMoves()
    {
        int counter = 0;
        foreach(Constants.Moves move in moves)
        {
            if (move != Constants.Moves.EMPTY)
                counter++;
        }

        return counter;
    }

    public Constants.Moves GetMove(int i)
    {
        return moves[i];
    }

    public void ResetMoves()
    {
        currentMoveCounter = 0;
        moves = new Constants.Moves[4];
    }

    public void Die()
    {
        if (dead)
            return;
        dead = true;
        GetComponent<Animator>().StopPlayback();
        GetComponent<Rigidbody2D>().simulated = false;
        SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer renderer in renderers)
        {
            renderer.enabled = false;
        }
        CapsuleCollider2D[] colliders = GetComponentsInChildren<CapsuleCollider2D>();
        foreach( CapsuleCollider2D collider in colliders)
        {
            collider.enabled = false;
        }
        Instantiate(head, new Vector3(transform.position.x, transform.position.y + 1.85f, 0), Quaternion.identity).GetComponent<Rigidbody2D>().AddTorque(5000);
        turnBasedController.SetState(Constants.States.PLAYERDEAD);
        blood.Play();
    }

    public void End()
    {
        ani.SetBool("End", true);
    }

}
