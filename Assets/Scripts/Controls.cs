﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{

    private PlayerController player;
    private TurnBasedController turnBasedController;

    // Use this for initialization
    void Start()
    {
        player = GetComponent<PlayerController>();
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(turnBasedController.GetCurrentState())
        {
            case Constants.States.PLAYERCHOOSING: UpdatePlayerChoosing();
                break;
            default: break;
        }
    }

    void UpdatePlayerChoosing()
    {

        if (Input.GetKey(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.D))
                player.SetCurrentMoveTemp(Constants.Moves.UPRIGHT);
            else if (Input.GetKey(KeyCode.A))
                player.SetCurrentMoveTemp(Constants.Moves.UPLEFT);
            else
                player.SetCurrentMoveTemp(Constants.Moves.UP);

        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (Input.GetKey(KeyCode.D))
                player.SetCurrentMoveTemp(Constants.Moves.SLIDERIGHT);
            else if (Input.GetKey(KeyCode.A))
                player.SetCurrentMoveTemp(Constants.Moves.SLIDELEFT);
            else
                player.SetCurrentMoveTemp(Constants.Moves.DOWN);

        }
        else if (Input.GetKey(KeyCode.A))
        {

            player.SetCurrentMoveTemp(Constants.Moves.LEFT);

        }
        else if (Input.GetKey(KeyCode.D))
        {

            player.SetCurrentMoveTemp(Constants.Moves.RIGHT);
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            player.SetCurrentMoveTemp(Constants.Moves.DONOTHING);
        }
        else
        {
            player.SetCurrentMoveTemp(Constants.Moves.EMPTY);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            player.SetNextMove();
        }

    }
}
