﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadController : MonoBehaviour, InterestedInPlayerInterace
{

    private CircleCollider2D circleCollider;
    private Rigidbody2D rig;
    private bool getSmaller;
    private TurnBasedController turnBasedController;
    // Use this for initialization
    void Start()
    {
        circleCollider = GetComponent<CircleCollider2D>();
        rig = GetComponent<Rigidbody2D>();
        Camera.main.GetComponent<TurnBasedController>().AddTolist(this);
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (circleCollider.radius - Time.deltaTime / 15f > 0 && getSmaller)
        {
            circleCollider.radius -= Time.deltaTime / 15;
        }

    }

    public void FreezeZ()
    {
        rig.freezeRotation = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Spikes")
            getSmaller = true;
    }

    public void SetNewPlayer(GameObject player)
    {
    }

    public void Load()
    {
        if (gameObject != null)
        {
            turnBasedController.RemoveFromList(gameObject);
            Destroy(gameObject);
        }
    }

    public void Save()
    {

    }
}
