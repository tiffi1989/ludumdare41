﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShadeController : MonoBehaviour, InterestedInPlayerInterace {

    Transform playerTrans;
    private TurnBasedController turnBasedController;
    private SpriteRenderer[] renderers;
    
    // Use this for initialization
    void Start () {
        playerTrans = GameObject.Find("Player").transform;
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        renderers = GetComponentsInChildren<SpriteRenderer>();
        turnBasedController.AddTolist(this);

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (turnBasedController.GetCurrentState() == Constants.States.PLAYERCHOOSING)
        {
            transform.position = new Vector3(playerTrans.position.x + 10, playerTrans.position.y, playerTrans.position.z);
            foreach (SpriteRenderer renderer in renderers)
            {
                renderer.enabled = true;
            }
        }
        else
        {
            foreach(SpriteRenderer renderer in renderers)
            {
                renderer.enabled = false;
            }
        }
	}

    public void Load()
    {
    }

    public void Save()
    {
    }

    public void SetNewPlayer(GameObject player)
    {
        playerTrans = player.transform;
    }

}
