﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScript : MonoBehaviour {

    TurnBasedController turnBasedController;

	// Use this for initialization
	void Start () {
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            turnBasedController.SetState(Constants.States.END);
            collision.gameObject.GetComponentInParent<PlayerController>().End();
        }
    }
}
