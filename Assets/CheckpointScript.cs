﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CheckpointScript : MonoBehaviour {

    private TurnBasedController turnBasedController;
    private GameSavedTextScript gameSavedScript;

	// Use this for initialization
	void Start () {
        turnBasedController = Camera.main.GetComponent<TurnBasedController>();
        gameSavedScript = GameObject.Find("GameSavedText").GetComponent<GameSavedTextScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            turnBasedController.Save();
            turnBasedController.SetXOfLastCheckpoint(transform.position.x);
            gameSavedScript.ShowText();
        }
    }

}
